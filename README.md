# HuckBerry joke generator

## Development

### Install Yarn & NodeJS

https://yarnpkg.com/lang/en/docs/install/
https://nodejs.org/en/

### Dependencies

- ruby 2.7.1
- rails
- psql >= 9.6

### Initial Setup

Ensure the above dependencies are installed on machine.

To get started run:

`$ bin/setup`

**_If the above runs without issue_**
`$ yarn` in the root of the repository to install node modules.

### Running App locally

`$ bin/webpack-dev-server` to run webpack (hotreload is enabled - means making visual changes with immediate feedback without refreshing browser.)
`$ bin/rails s`

**_If you have the [Foreman gem](https://github.com/ddollar/foreman)_**, there is a Procfile.
Simply run `foreman start` in root dir of project.

### Testing

`$ bin/rspec`
