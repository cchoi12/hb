# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MarkovGenerator, type: :service do
  let(:jokes) do
    [
      'What do you call a fly without wings? A walk.',
      'When my wife told me to stop impersonating a flamingo, I had to put my foot down.',
      'What do you call someone with no nose? Nobody knows.',
      'What time did the man go to the dentist? Tooth hurt-y.',
      'Why can’t you hear a pterodactyl go to the bathroom? The p is silent.',
      'How many optometrists does it take to change a light bulb? 1 or 2? 1... or 2?',
      'I was thinking about moving to Moscow but there is no point Russian into things.',
      'Why does Waldo only wear stripes? Because he doesn\'t want to be spotted.',
      'Do you know where you can get chicken broth in bulk? The stock market.',
      'I used to work for a soft drink can crusher. It was soda pressing.',
      'Why did the tomato blush? Because it saw the salad dressing.',
      'Did you hear about the guy whose whole left side was cut off? He\'s all right now.',
      'Why didn’t the skeleton cross the road? Because he had no guts.',
      'Q: What’s 50 Cent’s name in Zimbabwe? A: 400 Million Dollars.'
    ]
  end


  describe 'initialize' do
    let(:amount) { 5 }
    let(:subject) do
      MarkovGenerator.call(text: jokes, joke_count: amount)
    end

    it 'returns an array' do
      expect(subject.is_a?(Array)).to be true
    end

    it 'returns joke_count jokes' do
      expect(subject.size).to eq amount
    end

    it 'throws error if no jokes provided' do
      expect { MarkovGenerator.call([]) }.to raise_error(ArgumentError)
    end
  end
end
