# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MarkovTableGenerator, type: :service do
  let(:jokes) do
    [
      'Why did the tomato blush? Because it saw the salad dressing.',
      'Did you hear about the guy whose whole left side was cut off? He\'s all right now.',
      'Why didn’t the skeleton cross the road? Because he had no guts.'
    ]
  end

  describe 'initialize' do
    let(:subject) { MarkovTableGenerator }

    it 'throws an error when jokes is not passed in' do
      expect { subject.call({ not: 'an array' }) }.to raise_error(TypeError)
    end

    it 'accepts an array of jokes' do
      expect { subject.call(jokes) }.not_to raise_error
    end
  end

  describe 'attributes' do
    let(:subject) { MarkovTableGenerator.call(jokes) }

    it 'contains a table hash' do
      expect(subject.table).not_to be_empty
      expect(subject.table).to have_key('skeleton')
    end

    describe 'starters' do
      it 'contains a joke starters array' do
        expect(subject.joke_starters).not_to be_empty
        expect(subject.joke_starters).to include 'Why'
      end

      it 'contains a punchline starters array' do
        expect(subject.punchline_starters).not_to be_empty
        expect(subject.punchline_starters).to include 'Because'
      end
    end

    describe 'enders' do
      it 'contains a joke enders array' do
        expect(subject.joke_enders).not_to be_empty
        expect(subject.joke_enders).to include 'blush?'
      end

      it 'contains a punchline enders array' do
        expect(subject.punchline_enders).not_to be_empty
        expect(subject.punchline_enders).to include 'guts.'
      end
    end
  end
end
