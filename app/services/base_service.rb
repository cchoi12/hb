# frozen_string_literal: true

# DRY self.call for other services, inherit from this
class BaseService
  def self.call(*args, &block)
    new(*args, &block).run
  end
end
