# frozen_string_literal: true

# Generate a table object with following attributes:
#   table = MarkovTableGenerator.call(input_intext: [])
#   table.table #=>
#     { 'The' => ['cat', 'dog'], 'cat' => ['is', 'grooms'], 'dog' => ['sleeps', 'begs'] ... }
#   table.joke_starters #=> ['The', 'The', 'The']
#   table.joke_enders #=> ['always?', 'eats?']
#   table.punchline_starters #=> ['The', 'Because']
#   table.punchline_enders #=> ['sleeps.', 'snaps.']
#   table.max #=> n(average size for all strings in array)
class MarkovTableGenerator < BaseService
  attr_reader :table, :starters, :joke_starters, :joke_enders,
              :punchline_enders, :punchline_starters, :max

  def initialize(text)
    raise TypeError unless text.is_a? Array

    @jokes = text
    @table = {}
    @max = calculate_average_length(text)
    @joke_starters = []
    @joke_enders = []
    @punchline_starters = []
    @punchline_enders = []
  end

  def run
    @jokes.each { |joke| add_to_table(joke) }

    self
  end

  private

  def calculate_average_length(text_array)
    return 0 if text_array.empty?

    @max = text_array.sum(&:size) / text_array.size
  end

  def add_to_table(joke)
    parsed_joke = joke.split(' ')

    add_starts(joke)
    add_enders(joke)

    parsed_joke.each_with_index do |word, i|
      break if parsed_joke[i + 1].nil?

      @table[word] ||= []
      @table[word] << parsed_joke[i + 1]
    end
  end

  def add_starts(joke)
    joke_start = joke.split.first
    punchline_start = joke.match(/\?\s'?([a-zA-z'-]+)'?/)

    @joke_starters << joke_start if joke_start.eql?(joke_start.capitalize)
    @punchline_starters << punchline_start[1] unless punchline_start.nil?
  end

  def add_enders(joke)
    joke_endings = joke.scan(/\w+[?]+/)
    punchline_endings = joke.scan(/\w+[\.|\!]{1}/)

    @joke_enders.concat(joke_endings)
    @punchline_enders.concat(punchline_endings)
  end
end
