# frozen_string_literal: true

class ICanHazDadJokesFetcher < BaseService
  include HTTParty

  HEADERS = { 'Accept' => 'application/json' }.freeze

  def initialize(term = nil)
    @term = term
  end

  def run
    base_uri = 'https://icanhazdadjoke.com/search'
    base_uri += "?term=#{@term}" if @term

    response = HTTParty.get(base_uri, headers: HEADERS)

    if response.success?
      response['results'].map { |result| result['joke'] }
    else
      []
    end
  end
end
