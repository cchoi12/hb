# frozen_string_literal: true

# Initializes with an array of jokes
#   MarkovGenerator.call(text: jokes_array, joke_count: n)
#   - `text:` is the named param for array of jokes
#   - `joke_count:` integer for how many jokes it should generate
# It then passes the array to the MarkovTableGenerator to parse the jokes into a table.
# After it generates the table, it will generate a joke using the table's attributes.
class MarkovGenerator < BaseService
  attr_reader :table, :max, :joke_starters, :joke_count

  def initialize(text:, joke_count:)
    @word_dictionary = MarkovTableGenerator.call(text)
    @joke_count = joke_count
    @table = @word_dictionary.table
    @max = @word_dictionary.max
    @joke_starters = @word_dictionary.joke_starters
  end

  def run
    raise ArgumentError, 'Word dictionary required' unless @word_dictionary

    create_joke_collection
  end

  private

  def create_joke_collection
    jokes = []

    # Generate n jokes based on what joke_count is.
    joke_count.times do
      jokes << generate_joke
    end

    # Remove duplicates.
    jokes.uniq
  end

  def generate_joke
    current = joke_starters.sample
    result = []
    result << current

    # Length of resulting joke should be <= max(calculated in table generator)
    while result.length <= max
      # Break if it hits the end or a word with no words in it's array.
      break if table[current].nil?

      # Sample the next word from the current word's next possible words.
      next_word = table[current].sample

      result << next_word
      # Save current for the next iteration.
      current = next_word
    end

    result.join(' ')
  end
end
