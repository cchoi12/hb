import React, { useState, useEffect } from "react";

import "../stylesheets/jokes.scss";

const JokeGenerator = () => {
  const [currentJoke, setCurrentJoke] = useState(
    "Let's put a smile on that face! If it makes sense that is..."
  );
  const [term, setTerm] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  const [jokes, setJokes] = useState([]);
  const [jokeIndex, setJokeIndex] = useState(0);
  const [jokesCount, setJokesCount] = useState(0);
  const baseUri = "/generate_joke";
  const termUri = `/generate_joke?term=${term}`;

  useEffect(() => {
    fetchJokesApi();
  }, []);

  const fetchJokesApi = () => {
    let url = baseUri;

    if (term) {
      url = termUri;
      setSearchTerm(term);
    }

    fetch(url)
      .then(response => response.json())
      .then(jokes => setJokesAndCounters(jokes))
      .catch(error => setCurrentJoke(error));
  };

  const setJokesAndCounters = jokes => {
    setJokes(jokes);
    setJokeIndex(0);
    setJokesCount(jokes.length);
  };

  const getJoke = () => {
    if (jokeIndex + 1 === jokesCount || term !== searchTerm) {
      fetchJokesApi();
    }

    setCurrentJoke(jokes[jokeIndex]);
    setJokeIndex(jokeIndex + 1);
  };

  const jokeCounterCheck = () => {};

  return (
    <div id="joke-wrapper">
      <div id="joke-display">{currentJoke}</div>
      <button onClick={() => getJoke()}>generate</button>
      <input
        type="text"
        placeholder="Optional term"
        value={term}
        onChange={e => setTerm(e.target.value)}
      />
    </div>
  );
};

export default JokeGenerator;
