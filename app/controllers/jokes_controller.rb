# frozen_string_literal: true

# #show maps to /generate_joke with an optional term param \
# to search the icanhazdadjokes api for jokes w/ term.
class JokesController < ApplicationController
  before_action :load_joke, only: :show

  def show
    if @jokes
      render json: @jokes, status: :ok
    else
      render json: { error: '404 Not Found' }, status: :not_found
    end
  end

  private

  def load_joke
    jokes = ICanHazDadJokesFetcher.call(params[:term])
    @jokes = MarkovGenerator.call(text: jokes, joke_count: 10)
  end
end
